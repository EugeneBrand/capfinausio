module ApplicationModule {
    'use strict';

    export interface IApplication {

        "loan" : {
            loan_Reason_Type_Id : number,
            reason_Detail : String,
            title_Id : number,
            first_Name : String,
            surname : String,
            date_Of_Birth : String,
            iD_Type_Id : number,
            iD_Number : String,
            mobile_Number : String,
            home_Tel_Number : String,
            email_Address : String,
            web_Product_Id : number
        },
        "address" : {
            residential_Status_Id : number,
            time_At_Address_Id : number,
            unit : String,
            street_Number : String,
            street_Name : String,
            street_Type : String,
            suburb : String,
            state : String,
            post_Code : String,
            landlord_Name : String,
            landlord_Contact_Number : String
        },
        "income" : {
            employment_Status_Id : number,
            name_Of_Employer : String,
            employer_Contact_Number : String,
            length_Of_Employment_Id : number,
            income_Frequency_Id : number,
            next_Pay_Date : String,
            employee_Number : String,
            receive_Centrelink_Benefits : Boolean,
            marital_Status_Id : number,
            sole_Income_Earner  : Boolean,
            number_Of_Dependents_Id : number,
            work_Income : number,
            government_Benefits_Income : number,
            rent_Or_Mortgage : number,
            food : number,
            loan_Repayments : number,
            other_Living_Expenses : number
        }

    }
}