import { FormControl } from '@angular/forms';

export class PhoneNumberValidator {

    static isValid( control : FormControl ): any {

        if( isNaN( control.value ) ) {
            return {
                "Not a number" : true
            };
        }

        return null;
    }

}