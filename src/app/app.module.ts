import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// Pages
import { LoansPage } from './../pages/loans/loans';
import { SettlementPage } from './../pages/settlement/settlement';
import { ApplicationsPage } from './../pages/applications/applications';
import { SettingsPage } from './../pages/settings/settings';
import { SecurityPage } from './../pages/security/security';
import { ProgressCircleComponent } from '../components/progress-circle/progress-circle';
import { HomePage } from './../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { SignUpPage } from './../pages/sign-up/sign-up';
import { ProfilePage } from './../pages/profile/profile';

// providers
import { AuthService } from './../providers/auth-service';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    SignUpPage,
    LoansPage,
    SettlementPage,
    ApplicationsPage,
    SettingsPage,
    SecurityPage,
    ProfilePage,
    ProgressCircleComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    SignUpPage,
    LoansPage,
    SettlementPage,
    ApplicationsPage,
    SettingsPage,
    SecurityPage,
    ProfilePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, AuthService]
})
export class AppModule {}
