
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, NavController, Events, MenuController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { App } from 'ionic-angular';
import 'rxjs/Rx';
// import md5 from 'crypto-md5';
// Pages
import { LoansPage } from './../pages/loans/loans';
import { SettlementPage } from './../pages/settlement/settlement';
import { ApplicationsPage } from './../pages/applications/applications';
import { SettingsPage } from './../pages/settings/settings';
import { SecurityPage } from './../pages/security/security';
import { LoginPage } from './../pages/login/login';
import { HomePage } from './../pages/home/home';
import { ProfilePage } from './../pages/profile/profile';
// Providers
import { AuthService } from './../providers/auth-service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage : any = LoginPage;
  activePage : any;
  profile = [];
  // profilePicture : any = "https://www.gravatar.com/avatar/";
  // usermail : any;

  pages: Array<{ title : string, component: any, icon : string }>;

  constructor( public platform: Platform, public authService : AuthService,
                public alertCtrl : AlertController, public event : Events, public menu : MenuController ) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: "list" },
      { title: 'My Loans', component: LoansPage, icon: "podium" },
      { title: 'Settlement', component: SettlementPage, icon: "stats" },
      { title: 'Applications', component: ApplicationsPage, icon: "clipboard" },
      { title: 'Profile', component: ProfilePage, icon: "contact" },
      { title: 'Security', component: SecurityPage, icon: "eye" },
      { title: 'Settings', component: SettingsPage, icon: "settings" }

    ];
    // ActivePage highlights
    this.activePage = this.pages[0];

    // Subscribe to LoginPage event to preload user profile
    this.event.subscribe( 'isLoggedIn', () => {
      this.getProfile();
    });

    // Subscribe to LoginPage to receive email for gravtar usage
    // this.event.subscribe( 'email', ( email ) => {
    //   this.usermail = email;
    //   console.log( this.usermail );
    //   this.profilePicture = "https://www.gravatar.com/avatar/" + md5( this.usermail.toLowerCase(), 'hex' );
    // });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  // Active page highlight on the side-menu
  checkActive(page) {
    return page == this.activePage;
  }

  /*
    Get user profile
  */
  getProfile() {
    this.authService.getProfile().then( result => {
      this.profile = result;
    });
  }


 /*
  Logout
 */
  logout() {    
    let alert = this.alertCtrl.create({
      title : 'Confirm Log Out',
      message : 'Are you sure you want to log out?',
      buttons : [
        {
          text : 'Cancel',
          role : 'cancel',
          handler : () => {
            console.log( 'Cancel clicked' );
          }
        },
        {
          text : 'Log Out',
          handler : () => {
            this.authService.logout();
            this.menu.close();
            this.nav.setRoot( this.rootPage );            
          }
        }
      ]
    });
    alert.present( prompt );
  }

}
