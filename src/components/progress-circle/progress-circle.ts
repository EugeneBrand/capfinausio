import { Component, Input } from '@angular/core';
import { ProgressBar } from 'progressbar.js';
/*
  Generated class for the ProgressCircle component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'progress-circle',
  templateUrl: 'progress-circle.html'
})
export class ProgressCircleComponent {

  // Receive input amount to render progress amount from parent component
  @Input( 'progress' ) progress;


  constructor() {

  }



}
