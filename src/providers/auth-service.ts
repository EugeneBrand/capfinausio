import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Transfer } from 'ionic-native';
import { Platform } from 'ionic-angular';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {

    // Member variables
    isLoggedin : boolean;
    AuthToken; // customer profile_token
    baseUrl : string = "http://apiukdmz.capfin.co.za";
    id : number; // customer_id
    prof_id : number; // profile_id

    // cordova
    cordova : any;
    // Native storage directory
    storageDirectory : string = '';

    constructor( public http : Http, public platform : Platform ) {
        this.http = http;
        this.isLoggedin = false;
        this.AuthToken = null;
        this.id = null;
        this.prof_id = null;

        // Check platform is ready before assigning transfer & storageDirectory
        this.platform.ready().then( () => {

            var fileTransfer;        

            if( this.platform.is( 'cordova' )) {
            return false;
            }

            if (this.platform.is('ios')) {
            this.storageDirectory = this.cordova.file.documentsDirectory;
            }
            else if(this.platform.is('android')) {
            this.storageDirectory = this.cordova.file.dataDirectory;
            }
            else {
            // exit otherwise, but you could add further types here e.g. Windows
            return false;
            }                  
        });              
    }

    storeUserCredentials( token, id, prof_id ) {
        window.localStorage.setItem( 'swarm', token );
        window.localStorage.setItem( 'id', id );
        window.localStorage.setItem( 'prof_id', prof_id );
        this.useCredentials( token, id, prof_id );
    }

    useCredentials( token, id, prof_id ) {
        this.isLoggedin = true;
        this.AuthToken = token;
        this.id = id;
        this.prof_id = prof_id;
    }

    loadUserCredentials() {
        var token = window.localStorage.getItem( 'swarm' );
        var id = window.localStorage.getItem( 'id' );
        var prof_id = window.localStorage.getItem( 'prof_id' );
        this.useCredentials( token, this.id, this.prof_id );
    }
    
    destroyUserCredentials() {
        this.isLoggedin = false;
        this.AuthToken = null;
        this.id = null;
        this.prof_id = null;
        window.localStorage.clear();
    }
    
    /* ------------------------------------------------- */
    /**********************-API METHODS- *****************/
    /* ------------------------------------------------- */

    //----------------- Auth ----------------------------//
    // --------------------------------------------------//

    // Get an auth token for every request
    getAuthToken() : Promise< any > {

        // Create headers
        var headers = new Headers( {
            "Content-Type" : "application/x-www-form-urlencoded", // Content is UrlEncoded hence the UrlEncode function
            "Authorization" : "Basic c3dhcm06b2F1dGgyLXN3YXJtLXNlY3JldA=="   // auth would have to be changed @ production time
        } );                                                                 // to use <client_id>:<secret>   

        // Request options
        let options = new RequestOptions( { headers : headers } );

        // Request content type is UrlEncoded 
        let body = this.jsonToURLEncoded( { "Grant_Type" : "client_credentials" } );        
        
        // Api Auth URL 
        var authUrl = "/oauth2/api/oauth2/token";

        // Http Request
        return this.http.post( this.baseUrl + `${ authUrl }`, body, options )
                        .map( res => res.json() )
                            .toPromise()
                                .catch( this.handleError );
         
    }

    //----------------- Profile -------------------------//
    // --------------------------------------------------//

    // Create a new profile / Signup !!!!!! => work out validation etc
    newProfile( profileData ) : Promise< any > {
        // Self 
        var new_profileData = profileData;
        // Get access token
        var prom = Promise.resolve( this.getAuthToken() )
                          .then( result => {
                              return result.access_token
                          });

        return prom.then( result => {
            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result }`
            });
            // RequestOptions
            var options = new RequestOptions( { headers: headers} );
            // Api url
            var newProfUrl = "/v1/profiles/profile";
            // Post client details
            return this.http.post( this.baseUrl + newProfUrl, new_profileData, options )
                            .map( ( response : Response ) => response.json() )
                                .toPromise()
                                    .catch( this.handleError )
                                        .then( res => {
                                            // store neccesary creds
                                            return true;
                                        });
        });                  
    }

    //
    updateProfile() {}

    //
    changePassword() {}

    // Retrieve customer profile
    getProfile() : Promise< any > {
        
        // Retrieve auth token
        var userProfile = Promise.resolve( this.getAuthToken() )
                           .then( result => {
                               return result.access_token;
                           });

        return userProfile.then( ( result ) => {

            // Headers
            var headers = new Headers( {
                "Authorization" : `bearer ${ result }`,
                "ProfileToken" : `${ this.AuthToken }`,
                "Cache-Control" : "no-cache"
            } );
            //console.log( this.AuthToken ); // Test
            // RequestOptions
            let options = new RequestOptions( { headers : headers } );
            //Request URL
            let profileUrl = `/v1/profiles/profile/${ this.prof_id }`; // profile_id 

            return this.http.get( this.baseUrl + profileUrl, options)
                    .map( (res : Response ) => res.json() )
                        .toPromise()
                            .then( result => {
                                //console.log( result );
                                return result;
                            }).catch( this.handleError );
        });
    }

    // Authenticate client login()
    authClient( user ) : Promise< any > {

        var token = Promise.resolve( this.getAuthToken() )
                           .then( ( result ) => {
                               //console.log( result );
                               return result.access_token;
                           });
       
        // User Auth URL
        var authProfileUrl = "/v1/profiles/profile/authenticate";

        // console.log( this.baseUrl + `${ authProfileUrl }`); // Test

        return token.then( ( result ) => { // result of above token promise
        
            // Set Headers
            var headers = new Headers( {
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result }`,
                "Cache-Control" : "no-cache"
            } );

            // Request Options
            var options = new RequestOptions( { headers : headers } );   
            
            // User Crentials 
            let usercreds =  `{\r\n\t\"email\": \"${ user.email }\",\r\n\t\"password\": \"${ user.password }\"\r\n}`;                  
            
            // Http request
            return this.http.post( this.baseUrl + `${ authProfileUrl }`, usercreds, options )
                     .map( res => res.json() )
                        .toPromise()
                                .then( ( result ) => {
                                    // console.log( result ); // Test
                                    this.storeUserCredentials( result.profile_token, result.customer_id, result.profile_id );
                                    return true;
                                }, reject => {
                                    return false;
                                }).catch( this.handleError );                                               
        });
    }
    
    //----------------- Customers ------------------------//
    // --------------------------------------------------//

    // Retrieve all customer details
    getCustomerDetails() : Promise< any > {

        var custPromise = Promise.resolve( this.getAuthToken() )
                                 .then( result => {
                                     return result;                           
                                    });

        return custPromise.then( result => {

            // Headers
            var headers = new Headers( { 
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`,
                "Access-Control-Allow-Origin" : "*"
            } );
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );

            // Customer Profiles URL
            var custProfileUrl = `/v1/customers/customer/${ this.id }`;

            return this.http.get( this.baseUrl + custProfileUrl, options )
                .map( ( response : Response ) => response.json() )
                    .toPromise()                           
                        .then( res => {
                            //console.log( res );
                            return res;
                        }).catch( this.handleError );
            });                         
    }

    // Retrieve customer personal details
    getPersonalDetails() : Promise< any > {
        
        var prom = Promise.resolve( this.getAuthToken() )
                          .then( res => {
                              return res;
                          });

        return prom.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });
            // RequestOptions
            var options = new RequestOptions( { headers : headers} );
            // Url
            var detailsUrl = `/v1/customers/customer/${ this.id }/personal_details`;

            return this.http.get( this.baseUrl + detailsUrl, options )
                       .map( res => res.json() )
                        .toPromise()
                            .then( res => {
                                //console.log( res );
                                return res;
                            }).catch( this.handleError );     
        });   
    }

    //
    updatePersonalDetails() {}

    // Retrieve address details
    getAddressDetails() : Promise< any > {

        var add = Promise.resolve( this.getAuthToken() )
                         .then( result => {
                             return result;
                         });

        return add.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var addUrl = `/v1/customers/customer/${ this.id }/address_details`;

            return this.http.get( this.baseUrl + addUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });                 
    }

    //
    updateAddressDetails() {}

    // Retrieve Communication preferences
    getCommunicationPrefs() : Promise< any > {

        var prefs = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return prefs.then( res => {
            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ res.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = `/v1/customers/customer/${ this.id }/comm_preferences`;

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );            
        });                    
    }

    //
    updateCommunicationPrefs() {}

    // Retrieve customer notifications
    getCustomerNotifs() : Promise< any > {

        var notif = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return notif.then( result => {
            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = `/v1/customers/customer/${ this.id }/notifications`;

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                        .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );    
        });                    
    }

    //
    getNotificationDetails() {}

    //
    markNotifAsRead() {}

    //----------------- Accounts ------------------------//
    // --------------------------------------------------//

    // Retrieve a list of customer accounts
    getAccountsList() : Promise< any> {

        var acc = Promise.resolve( this.getAuthToken() )
                            .then( result => {
                                return result;
                            });

        return acc.then( res => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ res.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/accounts/accounts";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );             
        });                     
    }

    // Retrieve detailed information for a specific account
    getSpecificAccount( account_id : number ) : Promise< any > {
        // Local var
        var account_id = account_id;
        
        var specAcc = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return specAcc.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = `/v1/accounts/account/${account_id}`;

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });   
    } 

    // Retrieve banking details for selected account 
    getAccountBankDetails( account_id : number ) : Promise< any > {
        // Local var
        var account_id = account_id;
        
        var accDet = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return accDet.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var detUrl = `/v1/accounts/account/${account_id}/bank_details`;

            return this.http.get( this.baseUrl + detUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });   
    }

    // Retrieve transaction details for a specific account
    getAccountTransactions( account_id : number ) : Promise< any > {
        // Local var
        var account_id = account_id;
        
        var trans = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return trans.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var transUrl = `/v1/accounts/account/${account_id}/transactions`;

            return this.http.get( this.baseUrl + transUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });
    }
  
    // Retrieve available to borrow ammount
    getAvailableBorrowAmmount( account_id : number ) : Promise< any > {
        // Local var
        var account_id = account_id;
        
        var borrow = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return borrow.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var borrowUrl = `/v1/accounts/account/${account_id}/available_to_borrow`;

            return this.http.get( this.baseUrl + borrowUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });
    } 

    // --> Add account documentations retrieval / submission methods

    // Email the customer an account statement
    emailStatement( account_id : number ) {
        // Local var
        var account_id = account_id;
        
        var emailStmt = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return emailStmt.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var stmtUrl = `/v1/accounts/account/${account_id}/statement/email`;

            return this.http.post( this.baseUrl + stmtUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return true;
                                }).catch( this.handleError );
        });         
    }

    // Download account statement to device
    downloadStatement( account_id : number ) {
        // Local var
        var account_id = account_id;
        
        var downloadStmt = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return downloadStmt.then( result => {

             // Headers
            var headers = new Headers({
                "Content-Type" : "application/pdf",
                "Authorization" : `bearer ${ result.access_token }`,
                "ProfileToken" : `${ this.AuthToken }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            options.responseType = ResponseContentType.Blob;

            let request = new RequestOptions( options );

            // Address url
            var dlUrl = `/v1/accounts/account/${account_id}/statement/download`;

            return this.http.get( this.baseUrl + dlUrl, request )
                        // .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );
        });        
    }
    // Testing download to native -- NOT WORKING!
    download( account_id : number ) {

        this.platform.ready().then( () => {

            const fileTransfer = new Transfer();
            var account_id = account_id;

        var downloadStmt = Promise.resolve( this.getAuthToken() )
                             .then( result => {
                                 return result;
                             });

        return downloadStmt.then( result => {

            var Optional = {

                headers : {
                    "Content-Type" : "application/pdf",
                    "Authorization" : `bearer ${ result.access_token }`,
                    "ProfileToken" : `${ this.AuthToken }`                    
                }
            }

            // Address url
            let url =  this.baseUrl + `/v1/accounts/account/${account_id}/statement/download`;

            fileTransfer.download( url, this.storageDirectory  + `capfin_statement_${ new Date().toLocaleDateString()}.pdf`, false, Optional )
                        .then( ( entry) => {
                            console.log( 'download complete: ' + entry.toURL());
                        }, ( error ) => {
                            this.handleError;
                        });
                        
            });   

        });
 
    }

    //
    submitDocs( account_id : number ){}


    //----------------- Application Process -------------//
    // --------------------------------------------------//

    //
    submitApplication( submission : any ) {

        // Submission object
        var data = submission;

        var submit = Promise.resolve( this.getAuthToken() )
                            .then( result => {
                                return result;
                            }).catch( this.handleError );

        return submit.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });
            // RequestOptions
            var options = new RequestOptions( { headers : headers} );
            // Url
            var sbmUrl = "/v1/applications/application";
            // Http
            return this.http.post( this.baseUrl + sbmUrl, data, options )
                            .map( res => res.json() )
                                .toPromise()
                                    .then( result => {
                                        return result;
                                    }).catch( this.handleError );

        });

    }

    //
    queryApplication( application_Id : number ) {}

    //
    cancelApplication() {}

    //----------------- Lookups -------------------------//
    // --------------------------------------------------//

    // Lookup employment statuses
    lookupEmployStatus() : Promise< any > {

        var emp = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return emp.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/employment_type";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });                    
    }

    // Lookup product
    lookupProduct() : Promise< any > {

        var prod = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return prod.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/product";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });             
    }

    // Lookup loan reason type
    lookupLoanReason() : Promise< any > {

        var loan = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return loan.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/loan_reason_type";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    //console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }

    // Lookup Id type
    lookupIdType() : Promise< any > {

        var id = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return id.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/identification_type";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }

    // Lookup income frquency
    lookupIncomeFreq() : Promise< any > {

        var inc = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return inc.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/frequency";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }

    // Lookup length of employment
    lookupLengthEmp() : Promise< any > {

        var emp = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return emp.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/length_of_employment";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }        

    // Lookup marital status
    lookupMaritalStat() : Promise< any > {

        var mar = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return mar.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/marital_status";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    } 

    // Lookup number of dependants
    lookupNumDepends() : Promise< any > {

        var dep = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return dep.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/number_of_dependents";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }  

    // Lookup residential status
    lookupResidentialStat() : Promise< any > {

        var res = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return res.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/residential_status";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }

    // Lookup time at address
    lookupTimeAtAaddress() : Promise< any > {

        var add = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return add.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/time_at_address";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }

    // Lookup title
    lookupTitle() : Promise< any > {

        var tit = Promise.resolve( this.getAuthToken() )
                            .then( res => {
                                return res;
                            });

        return tit.then( result => {

            // Headers
            var headers = new Headers({
                "Content-Type" : "application/json",
                "Authorization" : `bearer ${ result.access_token }`
            });    
            // RequestOptions
            var options = new RequestOptions( { headers : headers } );
            // Address url
            var prefUrl = "/v1/lookup/title";

            return this.http.get( this.baseUrl + prefUrl, options )
                        .map( res => res.json() )
                            .toPromise()
                                .then( res => {
                                    // console.log( res );
                                    return res;
                                }).catch( this.handleError );               
        });         
    }         


    /* ------------------------------------------------- */
    /****************** -Address Methods- *****************/
    /* ------------------------------------------------- */

    /**
     *  Search address
     */
    searchAddress( term : String ) {

        let searchTerm = term;

        var form = new FormData();
        form.append("api_key", "135e43f9-9bd9-4753-b39d-1889fa68af44");
        form.append("term", `${searchTerm}`);
        
        // headers
        var headers = new Headers({
            "contentType" : "false",
            "processData" : "false",
            "mimeType" : "multipart/form-data"
        });

        // RequestOptions
        var options = new RequestOptions( { headers : headers } );

        let searchUrl = "http://api.addressify.com.au/address/autoComplete"; 

        return this.http.post( searchUrl, form, options )
                        .map( res => res.json() )
                            .catch( this.handleError ); 

    }
    /**
     *  Validate chosen address
     */
    validateAddress( item : any ) {
        
        let address = item;

        var form = new FormData();
        form.append("api_key", "135e43f9-9bd9-4753-b39d-1889fa68af44");
        form.append("term", `${address}`);
        
        // headers
        var headers = new Headers({
            "contentType" : "false",
            "processData" : "false",
            "mimeType" : "multipart/form-data"
        });

        // RequestOptions
        var options = new RequestOptions( { headers : headers } );

        let searchUrl = "http://api.addressify.com.au/address/validate"; 

        return this.http.post( searchUrl, form, options )
                        .map( res => res.json() )
                            .catch( this.handleError ); 
        
    }
    /**
     *  Parse the chosen address
     */
    parseAddress( address : any ) {

        let term = address;

        var form = new FormData();
        form.append("api_key", "135e43f9-9bd9-4753-b39d-1889fa68af44");
        form.append("term", `${term}`);
        
        // headers
        var headers = new Headers({
            "contentType" : "false",
            "processData" : "false",
            "mimeType" : "multipart/form-data"
        });

        // RequestOptions
        var options = new RequestOptions( { headers : headers } );

        let searchUrl = "http://api.addressify.com.au/address/getParsedAddress"; 

        return this.http.post( searchUrl, form, options )
                        .map( res => res.json() )
                            .catch( this.handleError );
    }

    /* ------------------------------------------------- */
    /**********************-Helpers- *****************/
    /* ------------------------------------------------- */

    // Error handler
    private handleError( error : any ) : Promise< any > {
        console.error('An error occurred', error); // for test purposes only
        return Promise.reject(error.message || error);
    }

    // Convert a json object to the url encoded format of key=value&anotherkey=anothervalue
    private jsonToURLEncoded(jsonString){
        return Object.keys(jsonString).map(function(key){
        return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
        }).join('&');
    }

    // Logout button
    logout() {
        this.destroyUserCredentials();
    }

}


    