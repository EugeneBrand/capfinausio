import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ActionSheetController } from 'ionic-angular';
import { AuthService } from './../../providers/auth-service';
import * as FileSaver from 'file-saver';
import { File, Transfer } from 'ionic-native';

@Component({
  selector: 'page-loans',
  templateUrl: 'loans.html'
})
export class LoansPage {

  cordova: any;

  // Member vars
  account_No : any;
  bank_details = [];
  transactions = [];
  availableToBorrow = [];

  constructor( public navCtrl: NavController, public navParams: NavParams,
              public authService : AuthService, public actionSheetCtrl : ActionSheetController ) {



              }

  ionViewDidLoad() {
    // Get the accounts
    this.getAccounts();
  }

  /*
    Retrieve the account no then pass it to relevant methods
  */ 
  getAccounts() {
    Promise.resolve( this.authService.getAccountsList() ).then( result => {
      this.account_No = result[0].account_No;
      //console.log( this.account_No );
  }).then( () => {
      this.getAccountBankDetails();
      this.getAccountTransactions();
      this.getAvailableBorrowAmmount();
  });
  }

  /*
    Get users bank details
  */
  getAccountBankDetails() {
    Promise.resolve( this.authService.getAccountBankDetails( this.account_No ) ).then( result => {
      this.bank_details = result;
      //console.log( this.bank_details );
  });
  }
  /*
    Get list of all account transactions
  */
  
  getAccountTransactions() {
    Promise.resolve( this.authService.getAccountTransactions( this.account_No ) ).then( result => {
      this.transactions = result;
      //console.log( this.transactions );
    });
  }
  /*
    Get available to borrow information
  */
  getAvailableBorrowAmmount() {
    Promise.resolve( this.authService.getAvailableBorrowAmmount( this.account_No ) ).then( result => {
      this.availableToBorrow = result;
      //console.log( this.availableToBorrow );
    });
  }

  /*
    Email Statement
  */ 
  emailStatement() {
    Promise.resolve( this.authService.emailStatement( this.account_No ) ).then( result => {
      if( result ) {
        console.log( 'Email sent succesfully' );
      } else {
        console.log( 'Email could not be sent, please try again' );
      }
    });
  }

  /*
    Download Statement
  */ 
  downloadStatement() {

    //const fileTransfer = new Transfer();

    Promise.resolve( this.authService.download( this.account_No ) ).then( result => {
    console.log( 'download completed...' );

  // Promise.resolve( this.authService.download( this.account_No ) ).then( result => {
  //   console.log( 'download completed...' );
    // let file = result.blob();
    // console.log( file.size + " bytes file downloaded. File type: ", file.type);
    // FileSaver.saveAs( file, `capfin_statement_${new Date().toLocaleDateString()}.pdf`);
      
    });
  }

  /*
    Submit Document
  */ 
  submitDocumentation() {}

  /*
    ActionSheetController => drop-down account actions
  */ 
  presentActionSheet() {
    let actionsheet = this.actionSheetCtrl.create({
      title : 'Statements',
      buttons : [
        {
        text : 'Email statement',
        icon : 'mail',
        handler : () => {
          this.emailStatement();
        }
      }, {
        text : 'Download Statement',
        icon : 'download',
        handler : () => {
          this.downloadStatement();
        }
      }, {
        text : 'Submit Document',
        icon : 'arrow-dropup-circle',
        handler : () => { 
          this.submitDocumentation();
        }
      }
      ]
    });
    actionsheet.present();
  }


}
