import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth-service';
import { PhoneNumberValidator } from '../../validators/phone-number';
import { LoginPage } from './../login/login';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {

  @ViewChild('signupSlider') signupSlider: any;

  // Slide Form Members
  slideOneForm : FormGroup;
  slideTwoForm : FormGroup;
  slideThreeForm : FormGroup;

  // Boolean to check if user submitted
  submitAttempt : boolean = false;

  // Lookup members 
  loanReason = [];
  idType = [];
  incomeFrequency = [];
  lengthEmploy = [];
  maritalStatus = [];
  numDependents = [];
  residentialStatus = [];
  timeAtAddress= [];
  titles = [];
  prodDetails = [];
  employmentStatus = [];

  // Address
  address = [];
  chsnAddress = [];
  customerAddress = [];
  address_valid = false;
  shouldHide = false;

  // Loan application members
  loanAmount = [ "500", "750", "1000", "1250", "1500", "1750", "2000", "2005",
                 "2250", "2500", "2750", "3000", "3500", "4000", "4500", "5000" ];
  loanTerm = [];
  chsnAmount : any;
  chsnTerm : any;
  loanReasonId : any;
  prodId : any;
  weeklyAmnt : any;
  loanReasonParents = [];
  loanReasonChildren = [];
  loanChildItem = [];

  // On submit
  submissionObject = [];
  application_id : any;

  // Initialize member for loading indicator
  loading : any;  


  constructor(public navCtrl: NavController, public navParams: NavParams,
              public formBuilder : FormBuilder, public authService : AuthService,
              private loadingContr : LoadingController, private alertCtrl: AlertController  ) {

                this.lookupLoanReason();
                this.lookupTitles();
                this.lookupIdType();
                this.lookupIncomeFrequency();
                this.lookupLengthEmployment();
                this.lookupMaritalStatus();
                this.lookupNumDependents();
                this.lookupResidentialStatus();
                this.lookupTimeAtAddress();
                this.lookupProductDetails();
                this.lookupEmploymentStatus();

                // this.slideOneForm = formBuilder.group({
                //   loan_Reason_Type_Id : ['', Validators.compose([Validators.required])],
                //   reason_Detail : ['', Validators.compose([Validators.required])],
                //   title_Id : ['', Validators.compose([Validators.required])],
                //   first_Name : ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z]*'), Validators.required])],
                //   surname : ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z]*'), Validators.required])],
                //   date_Of_Birth : ['', Validators.compose([Validators.required])],
                //   iD_Type_Id : ['', Validators.compose([Validators.required])],
                //   id_Number : ['', Validators.compose([Validators.maxLength(13), Validators.required])],
                //   mobile_Number : ['', Validators.compose([Validators.maxLength(10), Validators.required, PhoneNumberValidator.isValid])],
                //   home_Tel_Number : ['', Validators.compose([Validators.maxLength(10), Validators.required, PhoneNumberValidator.isValid])],
                //   email_Address : ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'), Validators.required])],
                //   web_Product_Id : ['', Validators.compose([Validators.required])]
                // });

                // Test Form with no validation
                this.slideOneForm = formBuilder.group({
                  loan_Reason_Type_Id : [''],
                  reason_Detail : [''],
                  title_Id : [''],
                  first_Name : [''],
                  surname : [''],
                  date_Of_Birth : [''],
                  iD_Type_Id : [''],
                  id_Number : [''],
                  mobile_Number : [''],
                  home_Tel_Number : [''],
                  email_Address : [''],
                  web_Product_Id : ['']
                });                

                // this.slideTwoForm = formBuilder.group({
                //   residential_Status_Id :['', Validators.compose([Validators.required])],
                //   time_At_Address_Id : ['', Validators.compose([Validators.required])],
                //   unit : ['', Validators.compose([Validators.required])],
                //   street_Number : ['', Validators.compose([Validators.required])],
                //   street_Name : ['', Validators.compose([Validators.required])],
                //   street_Type : ['', Validators.compose([Validators.required])],
                //   suburb : ['', Validators.compose([Validators.required])],
                //   state :['', Validators.compose([Validators.required])],
                //   post_Code : ['', Validators.compose([Validators.required])],
                //   landlord_Name : ['', Validators.compose([Validators.required])],
                //   landlord_Contact_Number : ['', Validators.compose([Validators.required])]
                // });

                // Test Form with no validation
                this.slideTwoForm = formBuilder.group({
                  residential_Status_Id :[''],
                  time_At_Address_Id : [''],
                  unit : [''],
                  street_Number : [''],
                  street_Name : [''],
                  street_Type : [''],
                  suburb : [''],
                  state :[''],
                  post_Code : [''],
                  landlord_Name : [''],
                  landlord_Contact_Number : ['']
                });

                this.slideThreeForm = formBuilder.group({
                  employment_Status_Id : ['', Validators.compose([Validators.required])],
                  employer_Contact_Number : ['', Validators.compose([Validators.required])],
                  length_Of_Employment_Id : ['', Validators.compose([Validators.required])],
                  income_Frequency_Id : ['', Validators.compose([Validators.required])],
                  next_Pay_Date : ['', Validators.compose([Validators.required])],
                  employee_Number : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  receive_Centrelink_Benefits : ['', Validators.compose([Validators.required])],
                  marital_Status_Id : ['', Validators.compose([Validators.required])],
                  sole_Income_Earner  : ['', Validators.compose([Validators.required])],
                  number_Of_Dependents_Id : ['', Validators.compose([Validators.required])],
                  work_Income : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  government_Benefits_Income : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  rent_Or_Mortgage : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  food : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  loan_Repayments : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])],
                  other_Living_Expenses : ['', Validators.compose([Validators.required, PhoneNumberValidator.isValid])]
                });

              }

  ionViewDidLoad() {}

  next() {
    this.signupSlider.slideNext();
  }

  prev() {
    this.signupSlider.slidePrev();
  }

  /** 
   * Save form submission 
   */
  save() {

    this.submitAttempt = true;
    // Check if all forms are valid
    if(!this.slideOneForm.valid){
      this.signupSlider.slideTo(0);
    }
    else if(!this.slideTwoForm.valid) {
      this.signupSlider.slideTo(1);
    }
    else if(!this.slideThreeForm.valid) {
      this.signupSlider.slideTo(2);
    }
    else {
      // console.log('Form submitted...');
      // console.log( this.slideOneForm );
      // console.log( this.slideTwoForm );
      // console.log( this.slideThreeForm );

      this.showLoading();

      // Push the captured form details to submission array
      this.submissionObject.push({
        "loan" : this.slideOneForm,
        "address" : this.slideTwoForm,
        "income" : this.slideThreeForm
      });
      console.log( this.submissionObject );
      // Submit the form application to the authService
      //this.submitApplication();

      // temporary timeout untill submission is active
      setTimeout( ()=> {
        this.loading.dismiss();
        this.navCtrl.setRoot( LoginPage ); // temp => send user back to login after succesfull application submission        
      }, 2000);

    }

  }
  /**
   *  Submit the application
   */
  submitApplication() {
    Promise.resolve( this.authService.submitApplication( this.submissionObject )).then( result => {
      if( result ) {
        console.log('Submission successfull'); // testing
        this.application_id = result.application_id;
      }
      else {
        this.showError('Submission unsuccessfull');
      }
    });
  }

  /**
   *  Search Address
   */
  search( searchEvent ) {

    let term = searchEvent.target.value;
    //console.log(term);

    // if( term.trim().length >= 3 ) {
      this.authService.searchAddress( term ).subscribe( result => {
        this.address = result;
        //console.log( this.address );
      });
    // }
  }
  /**
   *  Validate and parse selected address
   */
  chooseItem( item : any ) {

    this.authService.validateAddress( item ).subscribe( result => {

      if( result && item.length !== 0 ) {
        this.chsnAddress = item;
        //console.log( this.chsnAddress );
        this.address_valid = true;
        this.authService.parseAddress( this.chsnAddress ).subscribe( result => {
          this.customerAddress = result; // save result to var
          //console.log( "Parsed Address: " + result ); // test
          this.shouldHide = true; //set hide to true
        });
      }
      else {
         this.address_valid = false; // If address not valid
      }
    });

  }

  /* ------------------------------------------------------------------------*/
  /* ----------------------------- LoanCalc ---------------------------------*/

  calcLoanTerms( LoanAmount ) {
    
    this.prodDetails.forEach(element => {
      //console.log(element.amount);
      if(element.amount == LoanAmount) {
        //alert(element.amount);
         this.loanTerm.push(element.term);
         //console.log( this.loanTerm );
         this.chsnAmount = LoanAmount;
      }
    });
  }

  calcLoanRepay( loanTerm ) {
    //console.log( loanTerm );
    this.prodDetails.forEach( element => {
      if(element.amount == this.chsnAmount && element.term == loanTerm ) {
        this.prodId = element.web_Product_Id;
        //console.log( element.web_Product_Id );
      }
    });
    //alert( this.prodId );
    let id = this.prodId;

    this.prodDetails.forEach( element => {
      if( element.web_Product_Id == id ) {
        this.weeklyAmnt = element.week;
      }         
    });
  }

  setLoanReason( loan_reason_id ) {
    //alert ( loan_reason_id );
    this.loanReasonId = loan_reason_id;
    this.loanReasonChildren.forEach( element => {
      //console.log( "Element: " + element );
      if( element.parent_Id == loan_reason_id ) {
        this.loanChildItem.push( element );
        //console.log( element.id );
      }
    });
  }

  /* ------------------------------------------------------------------------*/
  /* ----------------------------- Lookups ----------------------------------*/  

  /**
   * Lookup the reason types for the loan
   */
  lookupLoanReason() {
    Promise.resolve( this.authService.lookupLoanReason() ).then( result => {
      this.loanReason = result;
      //console.log( result );
      this.loanReason.forEach(element => {
        if( element.parent_Id == 0 ) {
          this.loanReasonParents.push( element );
          //console.log( this.loanReasonParents );
        } else {
          this.loanReasonChildren.push( element );
          //console.log( this.loanReasonChildren );
        }
      });

    });
  }
  /**
   * Lookup the Id types
   */
  lookupIdType() {
    Promise.resolve( this.authService.lookupIdType() ).then( result => {
      this.idType = result;
    });
  }
  /**
   * Lookup the frequecny of incomes
   */
  lookupIncomeFrequency() {
    Promise.resolve( this.authService.lookupIncomeFreq() ).then( result => {
      this.incomeFrequency = result;
    });
  }
  /**
   * Lookup the length of employment types
   */
  lookupLengthEmployment() {
    Promise.resolve( this.authService.lookupLengthEmp() ).then( result => {
      this.lengthEmploy = result;
    });
  }
  /**
   * Lookup marital status types
   */
  lookupMaritalStatus() {
    Promise.resolve( this.authService.lookupMaritalStat() ).then( result => {
      this.maritalStatus = result;
    });
  }
  /**
   * Lookup the number of dependent types
   */
  lookupNumDependents() {
    Promise.resolve( this.authService.lookupNumDepends() ).then( result => {
      this.numDependents = result;
      //console.log( this.numDependents );
    });
  }
  /**
   * Lookup the residential status types
   */
  lookupResidentialStatus() {
    Promise.resolve( this.authService.lookupResidentialStat() ).then( result => {
      this.residentialStatus = result;
    });
  }
  /**
   * Lookup time at address types
   */
  lookupTimeAtAddress() {
    Promise.resolve( this.authService.lookupTimeAtAaddress() ).then( result => {
      this.timeAtAddress = result;
    });
  }
  /**
   * Lookup title types
   */
  lookupTitles() {
    Promise.resolve( this.authService.lookupTitle() ).then( result => {
      this.titles = result;
      //console.log( this.titles );
    });
  }
  /**
   * Lookup the product detail types
   */
  lookupProductDetails() {
    Promise.resolve( this.authService.lookupProduct() ).then( result => {
      this.prodDetails = result;
    });
  }
  /**
   * Lookup the employment status types
   */
  lookupEmploymentStatus() {
    Promise.resolve( this.authService.lookupEmployStatus() ).then( result => {
      this.employmentStatus = result;
      //console.log( this.employmentStatus );
    });
  }

  /* ------------------------------------------------------------------------*/
  /* ----------------------------- Loading ----------------------------------*/  

    /*
    Loading spinner 
  */
  showLoading() {
    this.loading = this.loadingContr.create({
      spinner : 'crescent',
      showBackdrop : false
  });    
    this.loading.present();
  }

  /* 
    Display errors
  */
  showError( text ) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title : "Oops, something went wrong!",
      subTitle : text,
      buttons: ["OK"]
    });
    alert.present( prompt );
  }

}
