import { HomePage } from './../home/home';
import { SignUpPage } from './../sign-up/sign-up';
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, MenuController, Events } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  usercreds = {
    // Temporarily hardcoded values for testing purposes => empty when done
    email : 'eugene.brand@swarmdigital.co.za',
    password : 'password123'
  };

  // Initialize member for loading indicator
  loading : any;

  // Preload user profile and pass along to have available in sidemenu
  cust_profile = [];

  constructor(private navCtrl: NavController, private authService : AuthService,
              private loadingContr : LoadingController, private alertCtrl: AlertController,
              private menu : MenuController, public event : Events ) {
                // Disable sidemenu on login-page
                this.menu.enable( false, 'myMenu');
              }
  /*
   Log in
  */
  login( user ) {
    // Present loading indicator
    this.showLoading();   
    // Authenticate client details
    Promise.resolve( this.authService.authClient( user ) )
           .then( result => {

            if( result ) {
            setTimeout(() => {

              this.event.publish( 'isLoggedIn' );
              // this.event.publish( 'email', this.usercreds.email );        
              this.loading.dismiss();
              this.navCtrl.setRoot( HomePage );      
            });        
          } else {
            this.showError( "Access Denied" );
            this.navCtrl.setRoot( LoginPage );
          }
    });
  }

  /*
    Sends user to account creation page
   */
  createAccount() {
    this.navCtrl.push( SignUpPage ); //setroot or push??
  }

  /*
    Loading spinner 
  */
  showLoading() {
    this.loading = this.loadingContr.create({
      spinner : 'crescent',
      showBackdrop : false
  });    
    this.loading.present();
  }

  /* 
    Display errors
  */
  showError( text ) {
    setTimeout(() => {
      this.loading.dismiss();
    });

    let alert = this.alertCtrl.create({
      title : "Fail",
      subTitle : text,
      buttons: ["OK"]
    });
    alert.present( prompt );
  }



}
