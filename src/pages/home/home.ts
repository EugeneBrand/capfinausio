import { LoginPage } from './../login/login';
import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // Used to store the returned object
  accounts = [];
  _thisAccount = [];
  account_No : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public authService : AuthService, public menu : MenuController,
              public alertCtrl : AlertController ) {
                // Enable side-menu
                this.menu.enable( true, 'myMenu');
                // Get the account
                this.getAccounts();
              }

  ionViewDidLoad() {}

  /*
    Get user account 
  */
  getAccounts() {
   Promise.resolve( this.authService.getAccountsList() ).then( result => {
      this.account_No = result[0].account_No;
      //console.log( this.account_No );
      this.accounts = result;
      //console.log( result );
    }).then( () => {
      this.getAccountDetails();
    });
  }
  
  /*
    Get detailed account information
  */
  getAccountDetails() {
    Promise.resolve( this.authService.getSpecificAccount( this.account_No ) ).then( result => {
      this._thisAccount = result;
    });
  }

  // Implement progressbar.js to display current loan chart

  /*
    Calculate the current loan percetage for input to donut-chart
  */
  calculateLoanPercentage() {}  
 
}

 
