import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from './../../providers/auth-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from 'ionic-native';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  // private imageSrc : string;
  private imageSrc = "assets/img/chuck_avatar.jpg";  

  // Profile result
  cust_profile = [];
 
  // Update client profile
  updateProfileForm : FormGroup;
  formVisible : Boolean;
  // Lookup Id Type
  idType : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public authService : AuthService, public formBuilder : FormBuilder ) {

                this.lookupIdType();

                this.updateProfileForm = formBuilder.group({
                  profile_Id : [''],
                  email : [''],
                  first_name : [''],
                  surname : [''],
                  id_type : [''],
                  id_number : ['']
                });

              }

  ionViewDidLoad() {
    this.getProfile();
  }

  /**
   * Retrieve Customer profile
   */
  getProfile() {
    Promise.resolve( this.authService.getProfile() ).then( result => {
      this.cust_profile = result;
      //console.log( this.cust_profile );
    });
  }
  /**
   * Show / Hide update profile form on click
   */
  showForm() {
    if( this.formVisible === true) {
      this.formVisible = false;
    }
    else {
      this.formVisible = true;
    }
  }

  /*
   * Lookup the Id types
   */
  lookupIdType() {
    Promise.resolve( this.authService.lookupIdType() ).then( result => {
      this.idType = result;
    });
  }

  // User selected profile photo
  private openGallery(): void {
    let cameraOptions = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: Camera.DestinationType.FILE_URI,      
      quality: 100,
      targetWidth: 180,
      targetHeight: 180,
      encodingType: Camera.EncodingType.JPEG,      
      correctOrientation: true      
    }

    Camera.getPicture( cameraOptions )
      .then( file_uri => this.imageSrc = file_uri, 
      err => console.log(err));
  }  

 
}
